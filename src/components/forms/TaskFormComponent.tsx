import {
  ErrorMessage,
  Field,
  Form,
  Formik,
  FormikHelpers,
} from 'formik'
import * as Yup from 'yup'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { ADD_TASK, FILTER } from '../../features/task/taskSlice'


export const TaskFormComponent = () => {
  const dispatch = useAppDispatch()
  const { filter } = useAppSelector((selector) => selector.task)


  const newTaskSchema = Yup.object().shape(
    {
      text: Yup.string()
        .required('Text is required'),
    },
  )

  const initialValues = {
    text: '',
  }

  interface TaskValues {
    text: string
  }

  const onSubmitNewTask = (values: TaskValues, helpers: FormikHelpers<TaskValues>) => {
    if (values.text !== '') {
      dispatch(ADD_TASK(values.text))
      dispatch(FILTER(filter))
      helpers.resetForm()
    }
  }

  const getInputClass = (errors: string | undefined) => {
    let cssClass = 'form-field__input'
    if (errors !== undefined) {
      cssClass += ' form-field__input--invalid'
    }
    return cssClass
  }

  return (
    <div className='sub-container'>
      <fieldset className='fieldset'>
        <legend className='fieldset__legend'>Create a new task</legend>
        <Formik initialValues={ initialValues } onSubmit={ onSubmitNewTask } validationSchema={ newTaskSchema }>
          {
            ({ errors }) => (
              <Form className='form-container' >
                <div className='form-field'>
                  <Field id='text' name='text' className={ getInputClass(errors.text) } />
                  <label htmlFor='text' className='form-field__label'>Task Content</label>
                  <ErrorMessage name='text' component='div' className='error-message' />
                </div>
                <div className='text-center'>
                  <button type='submit' className='btn btn-success'>
                    Add New Task <FontAwesomeIcon icon={ faPlus } />
                  </button>
                </div>
              </Form>
            )
          }
        </Formik>
      </fieldset>
    </div>
  )
}
