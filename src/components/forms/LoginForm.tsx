import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  ErrorMessage,
  Field,
  Form,
  Formik,
} from 'formik'
import * as Yup from 'yup'
import { useNavigate } from 'react-router-dom'
import {
  faBan,
  faKey,
} from '@fortawesome/free-solid-svg-icons'
import { useAppDispatch } from '../../app/hooks'
import { SET_LOADING, SET_MESSAGE } from '../../features/main/mainSlice'
import { LOGIN } from '../../features/session/sessionSlice'


const LoginForm = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const loginSchema = Yup.object().shape(
    {
      username: Yup.string()
        .required('Email or username is required'),
      password: Yup.string().required('Password is required'),
    },
  )

  const initialValues = {
    username: '',
    password: '',
  }

  const actionLogin = async (values: { username: string, password: string }, helpers: { resetForm: () => void }) => {
    dispatch(SET_LOADING(true))
    new Promise<{ username: string }>((resolve, reject) => {
      setTimeout(() => {
        if (values.username === 'guest' && values.password === '1234') {
          // username = values.email
          resolve({ username: values.username })
        } else {
          reject()
        }
      }, 2000)
    }).then(({ username }) => {
      helpers.resetForm()
      dispatch(LOGIN(username))
      dispatch(SET_LOADING(false))
    }).catch(() => {
      dispatch(SET_LOADING(false))
      dispatch(SET_MESSAGE('Invalid username or password'))
    })
  }

  const getInputClass = (errors: string | undefined) => {
    let cssClass = 'form-field__input'
    if (errors !== undefined) {
      cssClass += ' form-field__input--invalid'
    }
    return cssClass
  }

  const goSingin = () => {
    navigate('/Singin', { replace:true })
  }

  return (
    <div className='container'>
      <div className='container-bordered container__centered'>
        <h2>Login Formik</h2>
        <hr />
        <Formik initialValues={ initialValues } onSubmit={ actionLogin } validationSchema={ loginSchema }>
          {
            ({ errors }) => {
              return (
                <Form >
                  <div className='form-field'>
                    <Field id='username' name='username' className={ getInputClass(errors.username) } />
                    <label htmlFor='username' className='form-field__label'>Username</label>
                    <ErrorMessage name='username' component='div' className='error-message' />
                  </div>
                  <div className='form-field'>
                    <Field id='password' name='password' type='password' className={ getInputClass(errors.password) } />
                    <label htmlFor='password' className='form-field__label'>Password</label>
                    <ErrorMessage name='password' className='error-message' component='div'></ErrorMessage>
                  </div>
                  <div className='row row--auto'>
                    <button type='button' className='btn btn-danger btn-block' onClick={ () => navigate('/', { replace: true }) }>
                      Cancel <FontAwesomeIcon icon={ faBan }/>
                    </button>
                    <button type='submit' className='btn btn-success btn-block'>
                      Login <FontAwesomeIcon icon={ faKey } />
                    </button>
                  </div>
                  <hr className='hr-or'/>
                  <div className='row row--auto'>
                    <button className='btn btn-block' type='button' onClick={ goSingin } >
                      Singin
                    </button>
                  </div>
                </Form>
              )
            }
          }
        </Formik>
      </div>
    </div>
  )
}

export default LoginForm
