import React, { useRef } from 'react'
import TaskComponent from '../pure/TaskComponent'
import { TaskFormComponent } from '../forms/TaskFormComponent'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSquare } from '@fortawesome/free-regular-svg-icons'
import { faSadCry } from '@fortawesome/free-solid-svg-icons'
import { DELETE_TASK, FILTER, Task, TaskFilter, UPDATE_TASK } from '../../features/task/taskSlice'
import { useAppDispatch, useAppSelector } from '../../app/hooks'


export const TaskListComponent = (): React.JSX.Element => {
  const dispatch = useAppDispatch()
  const { lstToShowTask, filter } = useAppSelector((selector) => selector.task)

  const filterOption = useRef<HTMLSelectElement>(null)

  const changeFilter = () => {
    if (filterOption.current !== null) {
      dispatch(FILTER(TaskFilter[filterOption.current.value as keyof typeof TaskFilter]))
    }
  }

  const toggleTask = (task: Task) => {
    if (filterOption.current !== null) {
      const taskUpdated = {
        ...task,
        completed: !task.completed,
      }
      dispatch(UPDATE_TASK(taskUpdated))
      dispatch(FILTER(filter))
    }
  }

  const deleteTask = (id: number) => {
    if (filterOption.current !== null) {
      dispatch(DELETE_TASK(id))
    }
  }

  return (
    <div className='container'>
      <h1 className='title'>Your Tasks</h1>
      <TaskFormComponent />
      <div className='row'>
        <label className='label'>Filter:</label>
        <select className='select-form' onChange={ changeFilter } ref={ filterOption }>
          {
            (Object.keys(TaskFilter) as Array<keyof typeof TaskFilter>).map((key) => (
              <option key={key} value={ key }>{ TaskFilter[key] }</option>
            ))
          }
        </select>
      </div>
      <h4>
        Please press on <FontAwesomeIcon icon={ faSquare } className='icon icon--btn icon--not-checked' /> when you complete your task
      </h4>
      {
        lstToShowTask.length > 0 ?
        <table className='table'>
          <thead>
            <tr className='table__row'>
              <th className='table__head-col'>id</th>
              <th className='table__head-col'>task</th>
              <th className='table__head-col'>state</th>
              <th className='table__head-col'>actions</th>
            </tr>
          </thead>
          <tbody>
            {
              lstToShowTask.map((task, index) => (
                <TaskComponent key={ index } onToggleTask={ () => toggleTask(task) } onDeleteTask={ () => deleteTask(task.id) } task={ task } />
              ))
            }
          </tbody>
        </table>
        : <>
          <div className='message'>
            <FontAwesomeIcon icon={ faSadCry } />
            You don't have any task jet, please add one or change the filter
          </div>
        </>
      }
    </div>
  )
}
