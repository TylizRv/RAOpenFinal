import PropTypes, { InferProps } from 'prop-types'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { SET_MESSAGE } from '../../features/main/mainSlice'


export enum MessageType {
  DANGER = 'DANGER',
  SUCCESS = 'SUCCESS',
  INFO = 'INFO',
}

const MessageProps = {
  // message: PropTypes.string.isRequired,
  type: PropTypes.oneOf( Object.keys(MessageType) as Array<keyof typeof MessageType> ).isRequired,
}

const MessageComponent = ({ type }: InferProps<typeof MessageProps>) => {
  const { message, messageTime } = useAppSelector((state) => state.main)
  const dispatch = useAppDispatch()

  setTimeout(() => {
    dispatch(SET_MESSAGE(''))
  }, messageTime)

  const getClassMessage = () => {
    let classCss = 'message '

    switch (type) {
      case MessageType.DANGER:
        classCss += ' message--error'
        break
      case MessageType.SUCCESS:
        classCss += ' message--success'
        break
      case MessageType.INFO:
        classCss += ' message--info'
        break
      default:
        classCss += ''
        break
    }

    if (message !== '') {
      classCss += ' message--show'
    }

    return classCss
  }

  return (
    <div className='row'>
      <div className={ getClassMessage() }>
        <span className='message__text'>{ message }</span>
      </div>
    </div>
  )
}

MessageComponent.propTypes = MessageProps

export default MessageComponent
