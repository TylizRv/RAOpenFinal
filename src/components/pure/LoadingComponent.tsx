
export const LoadingComponent = () => {
  return (
    <div className='loading'>
      <div className='loading__wheel loading__wheel--solid'></div>
      <span className='loading__text loading__text--after'>Loading</span>
    </div>
  )
}
