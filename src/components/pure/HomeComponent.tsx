import { faTasks } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { NavLink } from 'react-router-dom'
import { useAppSelector } from '../../app/hooks'

export const HomeComponent = () => {
  const isLogged = useAppSelector((state) => state.session.isLogged)

  return (
    <div className='container'>
      {
        isLogged?
        (<div className='menu-home'>
          <NavLink to='/Task' className='btn-menu-icon' children={
            <>
              <FontAwesomeIcon icon={ faTasks } className='btn-menu-icon__icon' />
              <span className='btn-menu-icon__text'>Task</span>
            </>
          }/>
        </div>)
        : (
          <>
            <h1>Welcome</h1>
            <p>Please login</p>
          </>
        )
      }
    </div>
  )
}
