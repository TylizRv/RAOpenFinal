import {
  configureStore,
} from '@reduxjs/toolkit'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import SessionReducer from '../features/session/sessionSlice'
import MainReducer from '../features/main/mainSlice'
import TaskReducer from '../features/task/taskSlice'


export const store = configureStore({
  reducer: {
    main: MainReducer,
    session: SessionReducer,
    task: TaskReducer,
  },
  middleware: [
    thunk,
    logger,
  ],
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
