import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
} from 'react-router-dom'

// Fontaweason
import '@fortawesome/fontawesome-svg-core/styles.css'

import {
  NotFoundPage,
} from './pages/404/NotFoundPage'
import LoginForm from './components/forms/LoginForm'
import NavContainer from './components/partials/NavContainer'
import { useAppSelector } from './app/hooks'

// Styles
import './styles/app.scss'
import { HomeComponent } from './components/pure/HomeComponent'
import { LoadingComponent } from './components/pure/LoadingComponent'
import MessageComponent, { MessageType } from './components/pure/MessageComponent'
import { TaskListComponent } from './components/containers/TaskListComponent'

function App() {
  const { isLoading } = useAppSelector((state) => state.main)
  const { isLogged } = useAppSelector((state) => state.session)

  const setLoader = () => {
    if (isLoading) {
      return (<LoadingComponent />)
    }
    return (<></>)
  }

  return (
    <BrowserRouter>
      <NavContainer />
      <Routes>
        <Route path='/'
          element={ <HomeComponent /> }
        />
        <Route path='/login'
          element={
            !isLogged ?
              <LoginForm /> :
              <Navigate to='/' />
          }
        />
        <Route path='/Task'
          element={
            isLogged
            ? <TaskListComponent />
            : <Navigate to='/' />
          }
        />
        <Route path='*' element={ <NotFoundPage /> } />
      </Routes>
      <MessageComponent type={ MessageType.DANGER } />
      { setLoader() }
    </BrowserRouter>
  )
}

export default App
