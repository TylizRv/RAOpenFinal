import {
  PayloadAction,
  createSlice,
} from '@reduxjs/toolkit'


interface MainState {
  isLoading: boolean
  message: string
  messageTime: number
}

const initialState: MainState = {
  isLoading: false,
  message: '',
  messageTime: 5000,
}

const MainSlice = createSlice({
  name: 'session',
  initialState,
  reducers: {
    SET_LOADING: (state: MainState, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload
    },
    SET_MESSAGE: (state: MainState, action: PayloadAction<string>) => {
      state.message = action.payload
    },
  },
})


export const {
  SET_LOADING,
  SET_MESSAGE,
} = MainSlice.actions

const MainReducer = MainSlice.reducer
export default MainReducer
