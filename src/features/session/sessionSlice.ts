import {
  PayloadAction,
  createSlice,
} from '@reduxjs/toolkit'


interface SessionState {
  isLogged: boolean
  username: string | null
}

const initialState: SessionState = {
  isLogged: localStorage.getItem('username')? true : false,
  username: localStorage.getItem('username'),
}

const SessionSlice = createSlice({
  name: 'session',
  initialState,
  reducers: {
    LOGIN: (state: SessionState, action: PayloadAction<string>) => {
      state.isLogged = true
      state.username = action.payload
      localStorage.setItem('username', action.payload)
    },
    LOGOUT: (state: SessionState) => {
      state.isLogged = false
      state.username = ''
      localStorage.clear()
    },
  },
})


export const {
  LOGIN,
  LOGOUT,
} = SessionSlice.actions

const SesionReducer = SessionSlice.reducer
export default SesionReducer
