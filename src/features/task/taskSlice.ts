import PropTypes from 'prop-types'
import {
  PayloadAction,
  createSlice,
} from '@reduxjs/toolkit'


export interface Task {
  id: number
  text: string
  completed: boolean
}

export const TaskProps = {
  id: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
}


export enum TaskFilter {
  SEE_ALL = 'SEE ALL',
  COMPLETED = 'COMPLETED',
  PENDING = 'PENDING',
}

export interface TaskState {
  lstTask: Task[]
  lstToShowTask: Task[]
  idLastTask: number
  filter: TaskFilter
}


export const taskInitialState: TaskState = {
  lstTask: [],
  lstToShowTask: [],
  idLastTask: 0,
  filter: TaskFilter.SEE_ALL,
}

export enum TaskActions {
  ADD_TASK = 'ADD_TASK',
  TOGGLE_TASK = 'TOGGLE_TASK',
  DELETE_TASK = 'DELETE_TASK',
  FILTER_TASKS = 'FILTER_TASKS',
}

const TaskSlice = createSlice(
  {
    name: 'task',
    initialState: taskInitialState,
    reducers: {
      ADD_TASK: (state: TaskState, action: PayloadAction<string>) => {
        state.idLastTask++
        state.lstTask.push({
          id: state.idLastTask,
          text: action.payload,
          completed: false,
        })
      },
      DELETE_TASK: (state: TaskState, action: PayloadAction<number>) => {
        state.lstTask = state.lstTask.filter((task) => task.id !== action.payload)
        state.lstToShowTask = state.lstToShowTask.filter((task) => task.id !== action.payload)
      },
      UPDATE_TASK: (state: TaskState, action: PayloadAction<Task>) => {
        state.lstTask = state.lstTask.map((task) => (
          task.id !== action.payload.id
          ? task
          : {
            ...task,
            text: action.payload.text,
            completed: action.payload.completed,
          }
        ))
      },
      FILTER: (state: TaskState, action: PayloadAction<TaskFilter>) => {
        state.filter = action.payload
        switch (action.payload) {
          case TaskFilter.COMPLETED:
            state.lstToShowTask = [...state.lstTask.filter((task) => task.completed)]
            break
          case TaskFilter.PENDING:
            state.lstToShowTask = [...state.lstTask.filter((task) => !task.completed)]
            break
          case TaskFilter.SEE_ALL:
            state.lstToShowTask = [ ...state.lstTask ]
            break
          default:
            state.lstToShowTask = []
            break
        }
      },
    },
  },
)

export const {
  ADD_TASK,
  DELETE_TASK,
  UPDATE_TASK,
  FILTER,
} = TaskSlice.actions

const TaskReducer = TaskSlice.reducer
export default TaskReducer
